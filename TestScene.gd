extends Node

export(PackedScene) var bullet_scene

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	# Reset the player position to StartPosition
	$Player.start($Position2D.position)


func _on_Player_fire():	
	var bullet = bullet_scene.instance()
	bullet.position = $Player.position
	var direction = $Player.rotation
	bullet.rotation = direction
	bullet.rotate(PI*1.5)
	
	var velocity = Vector2(500, 0.0)
	bullet.linear_velocity = velocity.rotated(bullet.rotation)
	
	add_child(bullet)
