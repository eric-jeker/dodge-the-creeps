extends Area2D

signal hit
signal fire

# Declare member variables here:
export var speed = 400
var screen_size


# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size
	hide()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	look_at(get_global_mouse_position())
	rotate(PI/2)
	
	var velocity = Vector2.ZERO
	if Input.is_action_pressed("move_up"):
		velocity.y -= 1
	if Input.is_action_pressed("move_down"):
		velocity.y += 1
	if Input.is_action_pressed("move_right"):
		velocity.x += 1
	if Input.is_action_pressed("move_left"):
		velocity.x -= 1
	
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed
		$AnimatedSprite.play()
	else:
		$AnimatedSprite.stop()

	position += velocity * delta
	position.x = clamp(position.x, 0, screen_size.x)
	position.y = clamp(position.y, 0, screen_size.y)
	
	if velocity.x != 0:
		# Move left or right
		$AnimatedSprite.animation = "walk"
		$AnimatedSprite.flip_v = false
		# If we move left we flip the sprite horizontaly so it looks left
		$AnimatedSprite.flip_h = velocity.x < 0
	else:
		$AnimatedSprite.animation = "up"
	
#
# _input triggers only once when the action is triggered
#	
func _input(event):
	if not $CooldownTimer.is_stopped(): return
	if Input.is_action_pressed("fire"):
		$CooldownTimer.start()
		emit_signal("fire")


func _on_Player_body_entered(body):
	# Player can't be hit by bullet
	if body.is_in_group("bullets"): return
	hide() # death animation
	emit_signal("hit")
	# Disable the collision physic so we can't hit the Player more than once
	$CollisionShape2D.set_deferred("disabled", true)


func start(pos):
	position = pos
	show()
	$CollisionShape2D.disabled = false
