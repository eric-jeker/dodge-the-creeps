extends CanvasLayer

signal start_game

# Welcome message
var message = "Dodge the Creeps!"


# Called when the node enters the scene tree for the first time.
func _ready():
	$Message.text = message
	$Message.show()

#
# Display a message in the middle of the HUD
#
# @param useTimer allows to disable the timer hiding the msg after 3 sec
#
func show_message(text, useTimer = true):
	$Message.text = text
	$Message.show()
	if useTimer:
		$MessageTimer.start()


func hide_message():
	$Message.hide()


func show_game_over():
	show_message("Game Over!")
	# Wait until the message timer timeout
	yield($MessageTimer, "timeout")
	
	# Display the Welcome message
	show_message(message, false)
	
	# Wait to display the button again
	yield(get_tree().create_timer(1), "timeout")
	$StartButton.show()
	$OpenScoreboardButton.show()


func update_score(score):
	$ScoreLabel.text = str(score)
		

func _on_StartButton_pressed():
	$StartButton.hide()
	$OpenScoreboardButton.hide()
	emit_signal("start_game")


func _on_MessageTimer_timeout():
	hide_message()


func _on_OpenScoreboard_pressed():
	$ScoreBoard.get_node("Control").show()


# ScoreBoard Signal
func close_scoreboard():
	$ScoreBoard.get_node("Control").hide()
