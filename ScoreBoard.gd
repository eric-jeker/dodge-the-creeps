extends CanvasLayer

signal close_scoreboard

const SQLite = preload("res://addons/godot-sqlite/bin/gdsqlite.gdns")

var db

# Called when the node enters the scene tree for the first time.
func _ready():
	prepare_database()
	load_scores()


func prepare_database():
	var dictionary: Dictionary = Dictionary()
	dictionary["id"] = {"data_type": "int", "primary_key": true, "not_null": true}
	dictionary["score"] = {"data_type": "int", "not_null": true}
	dictionary["date"] = {"data_type": "string", "not_null": true}
	
	var dir = Directory.new()
	dir.open("res://")
	dir.make_dir("data")
	
	db = SQLite.new()
	db.path = "res://data/score"
	db.verbosity_level = 0
	# Open the database using the db_name found in the path variable
	db.open_db()
	var res = db.create_table("score", dictionary)


func load_scores():
	var res = db.query("SELECT * FROM score ORDER BY score DESC LIMIT 10;")
	
	for node in $Control/VBoxContainer.get_children():
		$Control/VBoxContainer.remove_child(node)
	
	if db.query_result.size() == 0:
		var score_label = $Control/Score.duplicate()
		score_label.text = "No score so far!"
		score_label.show()
		$Control/VBoxContainer.add_child(score_label)
	
	for row in db.query_result:
		var score_label = $Control/Score.duplicate()
		score_label.text = row["date"] + ": " + str(row["score"])
		score_label.show()
		$Control/VBoxContainer.add_child(score_label)


func save_score(score):
	var new_score: Dictionary = Dictionary()
	var date = load("res://Date.gd").new().date('YYYY-MM-DD')
	new_score["date"] = date
	new_score["score"] = score
	db.insert_row("score", new_score)
	load_scores()


func _on_Button_pressed():
	emit_signal("close_scoreboard")
