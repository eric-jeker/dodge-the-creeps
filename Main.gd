extends Node

export(PackedScene) var mob_scene
export(PackedScene) var bullet_scene

# State of the game
var started = false
var score = 0
var countdown = 3

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()


# Start a new game
func new_game():
	# Reset the score
	score = 0
	countdown = 3
	# We remove all the mobs and bullets
	get_tree().call_group("mobs", "queue_free")
	get_tree().call_group("bullets", "queue_free")
	# Reset the player position to StartPosition
	$Player.start($StartPosition.position)
	# Start the timer, when it reaches zero the game starts
	$StartTimer.start()
	# Update the HUD
	$HUD.update_score(score)
	$HUD.show_message("Get Ready! (" + str(countdown) + ")", false)
	# Start the music
	$Music.play()
	# Hide the mouse cursor and set it as confined
	Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
	#Input.set_custom_mouse_cursor(preload("res://art/transparent-cursor.png"))


func game_over():
	started = false
	$ScoreTimer.stop()
	$MobTimer.stop()
	$HUD.show_game_over()
	$DeathSound.play()
	$Music.stop()
	$HUD.get_node("ScoreBoard").save_score(score)
	# Restore the mouse cursor
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	Input.set_custom_mouse_cursor(null)


# The game actually starts
func _on_StartTimer_timeout():
	countdown -= 1
	$HUD.show_message("Get Ready! (" + str(countdown) + ")", false)
	if countdown == 0:
		$HUD.hide_message()
		$MobTimer.start()
		$ScoreTimer.start()
		$StartTimer.stop()
		started = true


func _on_ScoreTimer_timeout():
	score += 1
	$HUD.update_score(score)


func _on_MobTimer_timeout():
	var mob = mob_scene.instance()
	
	var mob_spawn_location = get_node("MobPath/MobSpawnLocation")
	mob_spawn_location.offset = randi()
	
	# PI = 180°, we rotate the mob 90°, clockwise
	var direction = mob_spawn_location.rotation + PI / 2
	
	mob.position = mob_spawn_location.position
	
	# add randomness, add +/-45° of randomness
	direction += rand_range(-PI / 4, PI / 4)
	mob.rotation = direction
	
	var velocity = Vector2(rand_range(150, 250), 0.0)
	mob.linear_velocity = velocity.rotated(direction)
	
	mob.connect("hit", self, "_on_Mob_hit")
	
	add_child(mob)


func _on_Mob_hit():
	score += 1
	$HUD.update_score(score)


func _on_Player_fire():
	# can't shoot if game is not started
	if started == false: return
	
	var bullet = bullet_scene.instance()
	bullet.position = $Player.position
	var direction = $Player.rotation
	bullet.rotation = direction
	bullet.rotate(PI*1.5)
	
	var velocity = Vector2(500, 0.0)
	bullet.linear_velocity = velocity.rotated(bullet.rotation)
	
	add_child(bullet)
