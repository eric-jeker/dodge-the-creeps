extends RigidBody2D

signal hit

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimatedSprite.playing = true
	var mob_types = $AnimatedSprite.frames.get_animation_names()
	$AnimatedSprite.animation = mob_types[randi() % mob_types.size()]


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()


func _on_Mob_body_entered(body):
	if not body.is_in_group("bullets"): return
	hide() # death animation
	queue_free()
	# Disable the collision physic so we can't hit the Player more than once
	$CollisionShape2D.set_deferred("disabled", true)
	emit_signal("hit")
